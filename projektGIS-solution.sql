-- skidanje GIS podataka sa ferweb-a
-- preimenovanje tablica
ALTER TABLE "gis.osm_buildings_a_free_1" RENAME TO buildings;
ALTER TABLE "gis.osm_landuse_a_free_1" RENAME TO landuse;
ALTER TABLE "gis.osm_natural_free_1" RENAME TO nature;
ALTER TABLE "gis.osm_places_free_1" RENAME TO places;
ALTER TABLE "gis.osm_pofw_free_1" RENAME TO pofw;
ALTER TABLE "gis.osm_pois_free_1" RENAME TO pois;
ALTER TABLE "gis.osm_railways_free_1" RENAME TO railways;
ALTER TABLE "gis.osm_roads_free_1" RENAME TO roads;
ALTER TABLE "gis.osm_traffic_free_1" RENAME TO traffic;
ALTER TABLE "gis.osm_transport_free_1" RENAME TO transport;
ALTER TABLE "gis.osm_water_a_free_1" RENAME TO water;
ALTER TABLE "gis.osm_waterways_free_1" RENAME TO waterways;



-- pregledavanje ima li neispravnih geometrija
SELECT 'landuse', gid FROM landuse WHERE st_isvalid(geom) != TRUE UNION ALL
SELECT 'nature', gid FROM nature WHERE st_isvalid(geom) != TRUE UNION ALL
SELECT 'places', gid FROM places WHERE st_isvalid(geom) != TRUE UNION ALL
SELECT 'pofw', gid FROM pofw WHERE st_isvalid(geom) != TRUE UNION ALL
SELECT 'pois', gid FROM pois WHERE st_isvalid(geom) != TRUE UNION ALL
SELECT 'railways', gid FROM railways WHERE st_isvalid(geom) != TRUE UNION ALL
SELECT 'roads', gid FROM roads WHERE st_isvalid(geom) != TRUE UNION ALL
SELECT 'traffic', gid FROM traffic WHERE st_isvalid(geom) != TRUE UNION ALL
SELECT 'transport', gid FROM transport WHERE st_isvalid(geom) != TRUE UNION ALL
SELECT 'water', gid FROM water WHERE st_isvalid(geom) != TRUE UNION ALL
SELECT 'waterways', gid FROM waterways WHERE st_isvalid(geom) != TRUE;



-- skidanje GIS podataka sa http://www.diva-gis.org/datadown
-- raspakiravanje i unošenje u pgAdmin pomoću shp2pgsql-gui programa
-- preimenovanje novih relacija u pgAdminu
ALTER TABLE hrv_adm0 RENAME TO croatia;
ALTER TABLE hrv_adm1 RENAME TO croatian_counties;
ALTER TABLE hrv_adm2 RENAME TO croatian_places;



--provjeri jesu li geo podaci ispravni
SELECT * FROM croatia WHERE st_isvalid(geom) != TRUE;
SELECT * FROM croatian_counties WHERE st_isvalid(geom) != TRUE;
-- ličko-senjska županija nema ispravnu geometriju, ali ne bi trebala biti obrisana svejedno
SELECT * FROM croatian_places WHERE st_isvalid(geom) != TRUE;



--podešavanje SRID-a
SELECT UpdateGeometrySRID('croatia', 'geom', 3765);
SELECT UpdateGeometrySRID('croatian_counties', 'geom', 3765);
SELECT UpdateGeometrySRID('croatian_places', 'geom', 3765);
ALTER TABLE croatia ALTER COLUMN geom TYPE geometry(MultiPolygon,3765) USING ST_Transform(geom, 3765); 
ALTER TABLE croatian_counties ALTER COLUMN geom TYPE geometry(MultiPolygon,3765) USING ST_Transform(geom, 3765); 
ALTER TABLE croatian_places ALTER COLUMN geom TYPE geometry(MultiPolygon,3765) USING ST_Transform(geom, 3765); 



-------------------------------------------------------------------------------------------------
-------------------------------------------------------------------------------------------------
--
-- 1. zadatak
-- Spajanjem županija podijelite hrvatsku u četiri regije, prikažite te regije
--
--
-- dodavanje atributa region u relaciju croatian_counties, zatim ručno popunjavanje regije
ALTER TABLE croatian_counties ADD COLUMN region VARCHAR(100);

UPDATE croatian_counties
SET region = 'Nizinska Hrvatska'
WHERE gid = 19		-- Vukovarsko-srijemska
OR gid = 12		-- Osječko-baranjska
OR gid = 3 		-- Brodsko-posavsla
OR gid = 13 		-- Požeško-slavonska
OR gid = 18		-- Virovitičko-podravska
OR gid = 2; 		-- Bjelovarsko-bilogorska

UPDATE croatian_counties
SET region = 'Središnja Hrvatska'
WHERE gid = 15		-- Sisačko-Moslavačka
OR gid = 8		-- Koprivničko-križevačka
OR gid = 21 		-- Zagrebačka
OR gid = 5 		-- Grad Zagreb
OR gid = 9		-- Krapinsko-zagorska
OR gid = 17 		-- Varaždinska
OR gid = 11             -- Međimurska
OR gid = 7;		-- Karlovačka

UPDATE croatian_counties
SET region = 'Sjeverno primorje'
WHERE gid = 6 		-- Istarska
OR gid = 14		-- Primorsko-goranska
OR gid = 10; 		-- Ličko-senjska

UPDATE croatian_counties
SET region = 'Južno primorje'
WHERE gid = 20 		-- Zadarska
OR gid = 1		-- Šibensko-kninska
OR gid = 16		-- Splitsko-dalmatinska
OR gid = 4;		-- Dubrovačko-neretvanska



-- kreiranje relacije croatian_regions koja će predstavljati novi sloj na karti
-- sloj se sastoji od 4 poligona (hrvatske regije)
CREATE TABLE croatian_regions AS
SELECT region, ST_Union(ST_SnapToGrid(geom,0.0001)) AS geom
FROM croatian_counties
GROUP BY region;



-------------------------------------------------------------------------------------------------
-------------------------------------------------------------------------------------------------
-- 2. zadatak
-- Označite samo rijeku Savu. Kolika je duljina rijeke Save po vašim regijama?
--
--
-- izdvajanje save u novu relaciju
CREATE TABLE sava_waterway AS
SELECT * FROM waterways
WHERE lower(name) LIKE 'sava';



-- duljina save po regijama (presjek save i regije)
-- NAPOMENA: Sava prema korištenim kartama često prelazi granicu pa se čini da je puno
--           kraća nego što u stvari je.
-- Ukupna duljina save u Hrvatskoj je 303063.194739756 m
SELECT sum(st_length(st_intersection(sava_waterway.geom, croatia.geom)))
FROM sava_waterway, croatia



-- Duljina save po regijama kroz koje prolazi (2 regije)
SELECT croatian_regions.region, sum(st_length(st_intersection(sava_waterway.geom, croatian_regions.geom))) AS length_in_region
FROM sava_waterway, croatian_regions
WHERE st_intersects(sava_waterway.geom, croatian_regions.geom)
GROUP BY  croatian_regions.region;
-- Nizinska Hrvatska: 83106.2600560064 m 
-- Središnja Hrvatska: 219956.934931357 m
-- 303063.194739756 -  83106.2600560064 - 219956.934931357 = 0



-------------------------------------------------------------------------------------------------
-------------------------------------------------------------------------------------------------
-- 3. zadatak
-- Koliko bi se metara trebala izliti Sava da bude pogođena barem jedna zgrada (makar djelomično)?
-- Prikažite tu situaciju na karti (st_buffer).
-- Opišite kako ste se nosili s velikom količinom podataka, odnosno kako ste to riješili.
--
--
-- Najmanja udaljenost od save do neke zgrade
SELECT MIN(st_distance(sava_waterway.geom, pois.geom)) AS distance
FROM pois, sava_waterway
-- 17.4469514765233

--drugo rješenje
SELECT MIN(st_distance(sava_waterway.geom, buildings.geom)) AS distance
FROM buildings, sava_waterway
--54.2510689635261

-- poplava (buffer) 
CREATE TABLE sava_flood AS
SELECT 	sava_waterway.gid,
	st_buffer(sava_waterway.geom,
		(SELECT MIN(st_distance(sava_waterway.geom, pois.geom)) AS distance
		 FROM pois, sava_waterway
		),
		'endcap=flat join=round'
		) AS geom
FROM sava_waterway;

-- drugo rješenje
CREATE TABLE sava_flood_2 AS
SELECT 	sava_waterway.gid,
	st_buffer(sava_waterway.geom,
		(SELECT MIN(st_distance(sava_waterway.geom, buildings.geom)) AS distance
		 FROM buildings, sava_waterway
		),
		'endcap=flat join=round'
		) AS geom
FROM sava_waterway;


-- kreiraj sloj poplavljene zgrade (jedna)
CREATE TABLE flooded_buildings AS
SELECT pois.* 
FROM pois, sava_flood
WHERE st_intersects(pois.geom, sava_flood.geom);

-- drugo rješenje
CREATE TABLE flooded_buildings_2 AS
SELECT buildings.* 
FROM buildings, sava_flood_2
WHERE st_intersects(buildings.geom, sava_flood_2.geom);




-------------------------------------------------------------------------------------------------
-------------------------------------------------------------------------------------------------
-- 4. zadatak
--Prikažite županije u različitim nijansama zelene boje s obzirom na površinu šuma
--
--
--zbroj površina svih šuma je 22264964018.2682 iako su tu ubrojane i neke površine izvan rh

CREATE TABLE forest_areas_by_counties AS
SELECT croatian_counties.gid, croatian_counties.name_1, croatian_counties.geom, SUM(st_area(landuse.geom)) AS forest_area 
FROM  croatian_counties, landuse
WHERE st_within(landuse.geom, croatian_counties.geom)
AND landuse.fclass = 'forest'
GROUP BY croatian_counties.gid, croatian_counties.name_1, croatian_counties.geom
-- zbroj površina je 9960607506.31961
-- više nego duplo manje zato jer within uzima samo one šume koje se nalaze isključivo unutar županije


CREATE TABLE forest_areas_by_counties AS
SELECT croatian_counties.gid, croatian_counties.name_1, croatian_counties.geom, SUM(st_area(landuse.geom)) AS forest_area 
FROM  croatian_counties, landuse
WHERE st_intersects(landuse.geom, croatian_counties.geom)
AND landuse.fclass = 'forest'
GROUP BY croatian_counties.gid, croatian_counties.name_1, croatian_counties.geom;
-- zbroj je sada upola veći 31525930207.7711 zato jer za sve šume koje se nalaze na granici županija
-- ubraja cijelu površinu i u jednu i u drugu 


CREATE TABLE forest_areas_by_counties AS
SELECT	croatian_counties.gid, 
	croatian_counties.name_1, 
	croatian_counties.geom, 
	SUM(st_area(st_Intersection(landuse.geom, croatian_counties.geom))) AS forest_area 
FROM  croatian_counties, landuse
WHERE st_intersects(landuse.geom, croatian_counties.geom)
AND landuse.fclass = 'forest'
GROUP BY croatian_counties.gid, croatian_counties.name_1, croatian_counties.geom
--Query returned successfully: 21 rows affected, 05:57 minutes execution time.
-- zbroj površina je 19531837904.9596 što je vjerojatno pravi rezultat


SELECT SUM(st_area(st_intersection(landuse.geom, croatia.geom)))
FROM landuse, croatia
WHERE landuse.fclass = 'forest'



-------------------------------------------------------------------------------------------------
-------------------------------------------------------------------------------------------------
--5.zadatak
--Odredi područja u Hrvatskoj koja gravitiraju pojedinim gradovima s više od 30000 stanovnika 
CREATE TABLE croatian_big_cities AS
SELECT *
FROM places
WHERE population > 30000;

-- QGIS: Vector -> Geometry tools -> Voronoi polygons -> Input layer -> croatian_big_cities -> ok
-- gotov




























